
<div class="form-group">
<b><label class="control-label" for="CI">{{'CI'}}</label></b>
<input class="form-control {{$errors->has('CI')? 'is-invalid':''}}" type="text" name="CI" id="CI"
value="{{isset ($cliente->CI)?$cliente->CI:old('CI')}}"
>
{!! $errors->first('CI', '<div class="invalid-feedback">:message</div>
')!!}
</div>


<div class="form-gruop">
<b><label class="control-label" for="Name">{{'Name'}}</label></b>
<input class="form-control  {{$errors->has('Name')? 'is-invalid':''}}" type="text" name="Name" id="Name"
value="{{isset ($cliente->Name)?$cliente->Name:old('Name')}}"
>
{!! $errors->first('Name', '<div class="invalid-feedback">:message</div>
')!!}
</div>
<div class="form-gruop">
<b> <label class="control-label" for="Lastname">{{'Lastname'}}</label></b>
<input class="form-control {{$errors->has('Lastname')? 'is-invalid':''}}" type="text" name="Lastname" id="Lastname"
value="{{isset ($cliente->CI)?$cliente->Lastname:old('Lastname')}}"
>
{!! $errors->first('Lastname', '<div class="invalid-feedback">:message</div>
')!!}
</div>
<div class="form-gruop">
<b> <label class="control-label" for="Phone">{{'Phone'}}</label></b>
<input class="form-control {{$errors->has('Phone')? 'is-invalid':''}}" type="text" name="Phone" id="Phone"
value="{{isset ($cliente->CI)?$cliente->Phone:old('Phone')}}"
>
{!! $errors->first('Phone', '<div class="invalid-feedback">:message</div>
')!!}
</div>
<div class="form-gruop">
<b> <label class="control-label" for="Email">{{'Email'}}</label></b>
<input class="form-control {{$errors->has('Email')? 'is-invalid':''}}" type="email" name="Email" id="Email"
value="{{isset ($cliente->CI)?$cliente->Email:old('Email')}}"

>
{!! $errors->first('Email', '<div class="invalid-feedback">:message</div>
')!!}
</div>
<br>
<div class="form-gruop">
<b> <label  class="control-label" for="Foto">{{'Foto'}}</label></b>
@if(isset($cliente->Foto))
<br>
<img  class="img-thumbnail img-fluid" src="{{asset('storage').'/'.$cliente->Foto}}" alt="" width="200">
<br>
@endif
<input class="form-control {{$errors->has('Foto')? 'is-invalid':''}}"class="btn btn-dark" type="file" name="Foto" id="Foto">
{!! $errors->first('Foto', '<div class="invalid-feedback">:message</div>
')!!}
</div>
<br>

<input class="btn btn-dark" type="submit" value="{{$Modo=='crear' ? 'Agregar': 'Modificar'}}">
<a class="btn btn-danger" href="{{url('clientes')}}">Regresar</a>