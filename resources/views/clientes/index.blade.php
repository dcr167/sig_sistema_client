@extends('layouts.app')

@section('content')

<div class="container">
@if(Session::has('Mensaje'))
    <div class="alert alert-success" role="alert">
            {{ Session::get('Mensaje')}}
        </div>
        
@endif

<a href="{{url('clientes/create')}}" class="btn btn-success">Agregar Cliente</a>
<br>
<br>
<table class="table table-light table-hover" >
    <thead class="thead-light">
        <tr>
            <th>#</th>
            <th>CI</th>
            <th>Name Complete</th>
            <th>Phone</th>
            <th>Email</th>
            <th>Foto</th>
            <th>Operaciones</th>
        </tr>
    </thead>
    <tbody>
         @foreach($clientes as $cliente)
        <tr>
            <td>{{$loop->iteration}}</td>
            <td>{{$cliente->CI}}</td>
            <td>{{$cliente->Name}} {{$cliente->Lastname}}</td>
            <td>{{$cliente->Phone}}</td>
            <td>{{$cliente->Email}}</td>
            <td>
                <img src="{{asset('storage').'/'.$cliente->Foto}}" class="img-thumbnail img-fluid" alt="" width="100">
                
            </td>
            <td> 
                <a href="{{url('/clientes/'.$cliente->id.'/edit')}}" class="btn btn-primary">
                    Editar
                </a>
      
                <form method="post" action="{{url('/clientes/'.$cliente->id)}}" style="display:inline">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <button class="btn btn-dark" type="submit" onclick="return confirm('¿Borrar?');">Borrar</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>

</table>
{{$clientes->links()}}
</div>
@endsection