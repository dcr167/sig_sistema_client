<?php

namespace App\Http\Controllers;

use App\Clientes;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
class ClientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $datos['clientes']=Clientes::paginate(5);

        return view('clientes.index',$datos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('clientes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        //$datosCliente=request()->all();

        $campos=[
            'CI' => 'required|string|max:50',
            'Name' => 'required|string|max:50',
            'Lastname' => 'required|string|max:50',
            'Phone' => 'required|string|max:50',
            'Email' => 'required|email',
            'Foto' => 'required|max:10000|mimes:jpeg,png,jpg'

        ];
        $Mensaje=["required" =>'El :attribute es requerido'];
        $this->validate($request,$campos,$Mensaje);


        $datosCliente=request()->except('_token');

        if($request->hasFile('Foto')){
            $datosCliente['Foto']=$request->file('Foto')->store('uploads','public');

        }
        Clientes::insert($datosCliente);

        //return response()->json($datosCliente);
        return redirect('clientes')->with('Mensaje','Cliente agregado con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function show(Clientes $clientes)
    {
        //

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $cliente=Clientes::findOrFail($id);

        return view('clientes.edit',compact('cliente'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $campos=[
            'CI' => 'required|string|max:50',
            'Name' => 'required|string|max:50',
            'Lastname' => 'required|string|max:50',
            'Phone' => 'required|string|max:50',
            'Email' => 'required|email'
        ];

        if($request->hasFile('Foto')){
           $campos+=['Foto' => 'required|max:10000|mimes:jpeg,png,jpg'];
        }

        $Mensaje=["required" =>'El :attribute es requerido'];
        $this->validate($request,$campos,$Mensaje);
        

        $datosCliente=request()->except(['_token','_method']);

        
        if($request->hasFile('Foto')){

            $cliente=Clientes::findOrFail($id);

            Storage::delete('public/'.$cliente->Foto);
            $datosCliente['Foto']=$request->file('Foto')->store('uploads','public');

        }

        Clientes::where('id','=',$id)->update($datosCliente);

       //$cliente=Clientes::findOrFail($id);
        //return view('clientes.edit',compact('cliente'));
        return redirect('clientes')->with('Mensaje','Cliente modificado con exito');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Clientes  $clientes
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $cliente=Clientes::findOrFail($id);

       if (Storage::delete('public/'.$cliente->Foto)){

        Clientes::destroy($id);
       }

        
        return redirect('clientes')->with('Mensaje','Cliente eliminado con exito');

    }
}
